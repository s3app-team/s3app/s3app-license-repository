## [0.5.4](https://gitlab.com/s3app-team/s3app/s3app-license-repository/compare/v0.5.3...v0.5.4) (2024-03-25)


### Bug Fixes

* postgres config ([4fb3be0](https://gitlab.com/s3app-team/s3app/s3app-license-repository/commit/4fb3be036c6976a2a94c8821363cea7463d4a148))

## [0.5.3](https://gitlab.com/s3app-team/s3app/s3app-license-repository/compare/v0.5.2...v0.5.3) (2024-03-25)


### Bug Fixes

* postgres config ([9e5c9b4](https://gitlab.com/s3app-team/s3app/s3app-license-repository/commit/9e5c9b4d94914761becc0c40ae1642adff92b082))

## [0.5.2](https://gitlab.com/s3app-team/s3app/s3app-license-repository/compare/v0.5.1...v0.5.2) (2024-03-25)


### Bug Fixes

* postgres config ([1d25504](https://gitlab.com/s3app-team/s3app/s3app-license-repository/commit/1d255049858e0e4b1b78da3dbd1088c26d81b4c7))

## [0.5.1](https://gitlab.com/s3app-team/s3app/s3app-license-repository/compare/v0.5.0...v0.5.1) (2024-03-10)


### Bug Fixes

* docker file ([43d5b42](https://gitlab.com/s3app-team/s3app/s3app-license-repository/commit/43d5b4266b272e4ce1e8850d0cc554e26fdd6247))

# [0.5.0](https://gitlab.com/s3app-team/s3app/s3app-license-repository/compare/v0.4.0...v0.5.0) (2023-12-26)


### Features

* uuid поле ([4ba8f1e](https://gitlab.com/s3app-team/s3app/s3app-license-repository/commit/4ba8f1e307c49a9f9856754b1db02459887195db))

# [0.4.0](https://gitlab.com/s3app-team/s3app/s3app-license-repository/compare/v0.3.0...v0.4.0) (2023-12-25)


### Features

* Активация ключа ([d328eb7](https://gitlab.com/s3app-team/s3app/s3app-license-repository/commit/d328eb7e36554b506821db4e7abb47bfcf066d4c))

# [0.3.0](https://gitlab.com/s3app-team/s3app/s3app-license-repository/compare/v0.2.0...v0.3.0) (2023-12-22)


### Features

* Access control ([93100cd](https://gitlab.com/s3app-team/s3app/s3app-license-repository/commit/93100cde97ca7dd242adb1d7dc15067795e82971))

# [0.2.0](https://gitlab.com/s3app-team/s3app/s3app-license-repository/compare/v0.1.0...v0.2.0) (2023-12-15)


### Features

* new version ([3677e86](https://gitlab.com/s3app-team/s3app/s3app-license-repository/commit/3677e8669d9575cd17acc79e7f330e09bb1cbd53))

# 0.1.0 (2023-12-15)


### Features

* Репозиторий лицензий ([bbd2394](https://gitlab.com/s3app-team/s3app/s3app-license-repository/commit/bbd2394b9d1318a456ed9a4471992f733b35b5df))
