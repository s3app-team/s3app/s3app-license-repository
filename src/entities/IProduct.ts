import { IEntity } from './IEntity';

export interface IProduct extends IEntity {
  id: string;
  name: string;
  price: number;
  days: number;
}