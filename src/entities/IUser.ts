import { IEntity } from './IEntity';

export enum UserRole {
  ADMIN = 'admin',
  USER = 'user',
}

export interface IUser extends IEntity {
  _id: string;
  id: string;
  name: string;
  email: string;
  company: string;
  phone: string;
  acceptedPrivacy: boolean;
  password?: string;
  resetPasswordCode?: string;
  resetPasswordExpires?: Date;
  confirmEmail?: string;
  confirmEmailExpires?: Date;
  confirmEmailCode?: string;
  role: UserRole;
}

