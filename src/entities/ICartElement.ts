import { IEntity } from './IEntity';

export interface ICartElement extends IEntity {
  id: string;
  count: number;
  licenseKeyId: string;
  productId: string;
  userId: string;
  name: string;
}