import { IEntity } from './IEntity';

export interface IOrderElement extends IEntity {
  id: string;
  count: number;
  licenseKeyId: string;
  orderId: string;
  productId: string;
  name: string;
}