import { IEntity } from './IEntity';

export enum IPaymentStatus {
  error = 'error',
  open = 'open',
  success = 'success',
}

export interface IPayment extends IEntity {
  id: string;
  sum: number;
  status: IPaymentStatus;
  paidAt: Date;
  orderId: string;
  paymentId: string;
  formLink: string;
}