import { IEntity } from './IEntity';

export interface ILicenseKey extends IEntity {
  id: string;
  purchaseId: string;
  licenseKey: string;
  expireAt: Date;
  createdAt: Date;
  activated: boolean;
  activatedAt: Date;
  productId: string;
  userOwnerId: string;
  name: string;
}