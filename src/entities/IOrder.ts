import { IEntity } from './IEntity';

export interface IOrder extends IEntity {
  id: string;
  sum: number;
  isPaid: boolean;
  paidAt: Date;
  userId: string;
  paymentId: string;
}