import { ILicenseKey } from '../entities/ILicenseKey';
import { IProduct } from '../entities/IProduct';
import LicenseRepository from '../repositories/LicenseRepository';
import { BaseResolver } from './baseResolvers';

// const typeDefs = `
//   scalar Date

//   type LicenseKey {
//     id: String
//     purchaseId: String
//     licenseKey: String
//     expireAt: Date
//     createdAt: Date
//     activated: Boolean
//     activatedAt: Date
//   }

//   input LicenseKeyInput {
//     purchaseId: String
//     licenseKey: String
//     expireAt: Date
//   }

//   type Query {
//     getLicenseKeys: [LicenseKey]
//     getLicenseKey(licenseKey: String!): LicenseKey!
//   }

//   type Mutation {
//     addLicenseKey(input: LicenseKeyInput!): LicenseKey!
//     updateLicenseKey(licenseKey: String!, expire_time: Date!): LicenseKey!
//     activateLicenseKey(licenseKey: String!): LicenseKey!
//   }
// `;

// const resolvers = {
//   Query: {
//     getLicenseKeys: (): Promise<ILicenseKey[]> => {
//       return db.select().from('LicenseKey');
//     },
//     getLicenseKey: (_: any, args: { licenseKey: string }): Promise<ILicenseKey> => {
//       return db.select().from('LicenseKey').where({ licenseKey: args.licenseKey }).first();
//     },
//   },
//   Mutation: {
//     addLicenseKey: async (_: any, args: { input: Partial<ILicenseKey> }): Promise<ILicenseKey> => {
//       const id = await db('LicenseKey').insert({ 
//         ...args.input,
//         create_time: new Date(),
//       }).returning('id');
//       return db.select().from('LicenseKey').where({ id: id[0].id }).first();
//     },
//     updateLicenseKey: async (_: any, args: { licenseKey: string, expire_time: Date }): Promise<ILicenseKey> => {
//       await db('LicenseKey').where({ licenseKey: args.licenseKey }).update({ expire_time: args.expire_time });
//       return db.select().from('LicenseKey').where({ licenseKey: args.licenseKey }).first();
//     },
//     activateLicenseKey: async (_: any, args: { licenseKey: string }): Promise<ILicenseKey> => {
//       const licenseKey: ILicenseKey = await db.select().from('LicenseKey').where({ licenseKey: args.licenseKey }).first();
//       if (licenseKey.activated) {
//         throw new Error('License key already activated');
//       }
//       await db('LicenseKey').where({ licenseKey: args.licenseKey }).update({ activated: true, activate_time: new Date() });
//       return db.select().from('LicenseKey').where({ licenseKey: args.licenseKey }).first();
//     },
//   },
// };

class LicenseResolver extends BaseResolver {
  setResolvers(): void {

    this.setSchema(`
    scalar Date
  
    type LicenseKey {
      id: String
      purchaseId: String
      licenseKey: String
      expireAt: Date
      createdAt: Date
      activated: Boolean
      activatedAt: Date
      name: String
      product: Product
    }
  
    input LicenseKeyInput {
      purchaseId: String
      licenseKey: String
      expireAt: Date
      name: String
    }
  
    type Query {
      getLicenseKeys: [LicenseKey]
      getLicenseKey(licenseKey: String!): LicenseKey!
    }
  
    type Mutation {
      addLicenseKey(input: LicenseKeyInput!): LicenseKey!
      updateLicenseKey(licenseKey: String!, expireAt: Date!): LicenseKey!
      activateLicenseKey(licenseKey: String!): LicenseKey!
      renameLicenseKey(licenseKeyId: ID!, name: String!): LicenseKey!
      refreshLicenseKey(licenseKeyId: ID!): LicenseKey!
      sendLicenseKeyEmail(licenseKeyId: ID!, email: String!): Boolean!
    }
  `);

    this.addQueryResolver<{}, ILicenseKey[]>('getLicenseKeys', async () => {
      return new LicenseRepository().getAll();
    }, ['admin', 'marketplace', 'service']);
    
    this.addQueryResolver<{ licenseKey: string }, Partial<ILicenseKey>>('getLicenseKey', async ({ args }) => {
      let result: Partial<ILicenseKey>;
      try {
        result = await (await LicenseRepository.getByLicenseKey(args.licenseKey)).getData();
      } catch (e) {
        result = {
          activated: true,
          expireAt: new Date(0),
        };
      }
      return result;
    }, ['admin', 'marketplace', 'service']);

    this.addMutationResolver<{ input: Partial<ILicenseKey> }, ILicenseKey>('addLicenseKey', async ({ args }) => {
      return new LicenseRepository().create(args.input);
    }, ['admin', 'marketplace']);

    this.addMutationResolver<{ licenseKey: string, expireAt: Date }, ILicenseKey>('updateLicenseKey', async ({ args }) => {
      return (await LicenseRepository.getByLicenseKey(args.licenseKey)).updateLicenseKey(args.expireAt);
    }, ['admin', 'marketplace']);

    this.addMutationResolver<{ licenseKey: string }, ILicenseKey>('activateLicenseKey', async ({ args }) => {
      return (await LicenseRepository.getByLicenseKey(args.licenseKey)).activateLicenseKey();
    }, ['admin', 'service']);

    this.addMutationResolver<{ licenseKeyId: string, name: string }, ILicenseKey>('renameLicenseKey', async ({ args, user }) => {
      return new LicenseRepository(user, args.licenseKeyId).edit({ name: args.name });
    }, ['admin', 'user']);

    this.addMutationResolver<{ licenseKeyId: string }, ILicenseKey>('refreshLicenseKey', async ({ args, user }) => {
      return new LicenseRepository(user, args.licenseKeyId).refreshLicenseKey();
    }, ['admin', 'user']);

    this.addMutationResolver<{ licenseKeyId: string, email: string }, boolean>('sendLicenseKeyEmail', async ({ args, user }) => {
      return (new LicenseRepository(user, args.licenseKeyId).sendEmail(args.email));
    }, ['admin', 'user']);

    this.addEntityResolver<ILicenseKey, IProduct>('LicenseKey', 'product',
      async ({ user, parent }) => {
        return new LicenseRepository(user, parent.id).getProduct();
      });
  }
}

export const licenseResolver = new LicenseResolver();

// export { resolvers, typeDefs };