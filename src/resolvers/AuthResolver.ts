import { IUser } from '../entities/IUser';
import { AuthRepository } from '../repositories/AuthRepository';
import { ConfirmEmailRepository } from '../repositories/ConfirmEmailRepository';
import { ResetPasswordRepository } from '../repositories/ResetPasswordRepository';
import { UserRepository } from '../repositories/UserRepository';
import { BaseResolver } from './baseResolvers';

class AuthResolver extends BaseResolver {
  setResolvers(): void {
    this.setSchema(`
      input UserRegister {
        name: String!
        email: String!
        password: String!
        company: String
        phone: String
        acceptedPrivacy: Boolean!
      }
      type Query {
        checkResetPasswordCode(code: String!): UserResDto
      }
      type Mutation {
        signIn(name: String, email: String, password: String!): String!
        signUp(user: UserRegister!): UserResDto!
        changePassword(oldPassword: String!, newPassword: String!): UserResDto
        sendRecoverPasswordLink(email: String!): Boolean
        changePasswordAfterRecover(code: ID!, password: String!): Boolean
        sendEmailConfirmationLink(email: String!): Boolean
        confirmEmail(code: ID!): Boolean
        logOut: Boolean
      }
    `);

    this.addQueryResolver<{ code: string }, IUser>('checkResetPasswordCode', 
      async ({ args, user }) => new ResetPasswordRepository(user).checkResetPasswordCode(args.code),
      ['admin', 'user', 'guest'],
    );

    this.addMutationResolver<{ user: Partial<IUser> },
    Partial<IUser>>('signUp', 
      async ({ args, user }) => new AuthRepository(user).signUp(args.user),
      ['admin', 'user', 'guest']);

    this.addMutationResolver
    <{ oldPassword: string; newPassword: string }, IUser>(
      'changePassword', 
      async ({ args, user }) => new UserRepository(user, user.id).changePassword(args.oldPassword, args.newPassword), ['admin', 'user']);

    this.addMutationResolver<{ email: string; password: string }, string>('signIn',
      async ({ args, user }) => new AuthRepository(user).signIn(args.email, args.password),
      ['admin', 'user', 'guest']);
      
    this.addMutationResolver<{ code: string; password: string }, boolean>('changePasswordAfterRecover',
      async ({ args, user }) => new ResetPasswordRepository(user).changePasswordAfterRecover(args.code, args.password),
      ['admin', 'user', 'guest']);
      
    this.addMutationResolver<{ email: string }, boolean>('sendRecoverPasswordLink',
      async ({ args, user }) => new ResetPasswordRepository(user).sendRecoverPasswordLink(args.email),
      ['admin', 'user', 'guest']);

    this.addMutationResolver<{ email: string }, boolean>('sendEmailConfirmationLink',
      async ({ args, user }) => new ConfirmEmailRepository(user, user.id).sendEmailConfirmationLink(args.email),
      ['admin', 'user', 'guest']);

    this.addMutationResolver<{ code: string }, boolean>('confirmEmail',
      async ({ args, user }) => new ConfirmEmailRepository(user).ConfirmEmail(args.code),
      ['admin', 'user', 'guest']);

    this.addMutationResolver<{}, boolean>('logOut', async ({ user, session }) => {
      await new AuthRepository(user, session.id).logOut(session.id);
      return true;
    }, ['admin', 'user']);
  }
}

export const authResolver = new AuthResolver();
