import { ILicenseKey } from '../entities/ILicenseKey';
import { IOrder } from '../entities/IOrder';
import { IOrderElement } from '../entities/IOrderElement';
import { IPayment } from '../entities/IPayment';
import { IProduct } from '../entities/IProduct';
import { IUser } from '../entities/IUser';
import LicenseRepository from '../repositories/LicenseRepository';
import OrderRepository from '../repositories/OrderRepository';
import ProductRepository from '../repositories/ProductRepository';
import { UserRepository } from '../repositories/UserRepository';
import { BaseResolver } from './baseResolvers';

class OrderResolver extends BaseResolver {
  setResolvers(): void {
    this.setSchema(`

    type Order {
      id: ID
      sum: Float
      isPaid: Boolean
      paidAt: DateTime
      elements: [OrderElement]
      user: User
      successPayment: Payment
      payments: [Payment]
      createdAt: DateTime
      updatedAt: DateTime
    }

    type OrderElement {
      id: ID
      count: Int
      licenseKey: LicenseKey
      product: Product
      name: String
    }

    input OrderElementInput {
      productId: ID
      licenseKeyId: ID
      count: Int
      name: String
    }
    
    type Query {
      getOrder(id: ID!): Order
      getOrders: [Order]
    }
  
    type Mutation {
      buyOrder(id: ID!): Payment
      addOrderElement(orderId: ID! input: OrderElementInput!): OrderElement  
      deleteOrder(id: ID!): Boolean
    }
  `);

    this.addQueryResolver<{}, IOrder[]>('getOrders',
      ({ args, user }) => new OrderRepository(user).getAll(), ['admin']);

    this.addQueryResolver<{ id: string }, IOrder>('getOrder',
      ({ args, user }) => new OrderRepository(user, args.id).getData(), ['admin', 'user']);

    this.addMutationResolver<{ id: string }, IPayment>('buyOrder',
      async ({ args, user }) => {
        const orderRepository = new OrderRepository(user, args.id);
        return orderRepository.startBuy();
      }, ['admin', 'user']);

    this.addMutationResolver<{ orderId: string, input: IOrderElement }, IOrder>('addOrderElement',
      async ({ args, user }) => {
        const orderRepository = new OrderRepository(user, args.orderId);
        await orderRepository.addElement(args.input);
        return orderRepository.getData();
      }, ['admin', 'user']);

    this.addMutationResolver<{ id: string }, boolean>('deleteOrder',
      async ({ args, user }) => new OrderRepository(user, args.id).delete(), ['admin', 'user']);

    this.addEntityResolver<IOrder, IOrderElement[]>('Order', 'elements',
      async ({ parent, user }) => {
        return new OrderRepository(user, parent.id).getElements();
      });

    this.addEntityResolver<IOrderElement, ILicenseKey>('OrderElement', 'licenseKey',
      async ({ parent, user }) => {
        return new LicenseRepository(user, parent.licenseKeyId).getData();
      });

    this.addEntityResolver<IOrderElement, IProduct>('OrderElement', 'product',
      async ({ parent, user }) => {
        return new ProductRepository(user, parent.productId).getData();
      });

    this.addEntityResolver<IOrder, IUser>('Order', 'user',
      async ({ parent, user }) => {
        return new UserRepository(user, parent.userId).getData();
      });

    this.addEntityResolver<IOrder, IPayment>('Order', 'successPayment',
      async ({ parent, user }) => {
        return new OrderRepository(user, parent.id).getSuccessPayment();
      });

    this.addEntityResolver<IOrder, IPayment[]>('Order', 'payments',
      async ({ parent, user }) => {
        return new OrderRepository(user, parent.id).getPayments();
      });
  }
}

export const orderResolver = new OrderResolver();
