import { ILicenseKey } from '../entities/ILicenseKey';
import { IOrder } from '../entities/IOrder';
import { IOrderElement } from '../entities/IOrderElement';
import { IPayment } from '../entities/IPayment';
import { IProduct } from '../entities/IProduct';
import { IUser } from '../entities/IUser';
import LicenseRepository from '../repositories/LicenseRepository';
import OrderRepository from '../repositories/OrderRepository';
import PaymentRepository from '../repositories/PaymentRepository';
import ProductRepository from '../repositories/ProductRepository';
import { UserRepository } from '../repositories/UserRepository';
import { BaseResolver } from './baseResolvers';

class PaymentResolver extends BaseResolver {
  setResolvers(): void {
    this.setSchema(`
      enum PaymentStatus {
          error
          open
          success
      }

    type Payment {
      id: ID
      sum: Float
      formLink: String
      status: PaymentStatus
      paidAt: DateTime
      order: Order
      createdAt: DateTime
      updatedAt: DateTime
    }
    
    type Query {
      getPayment(id: ID!): Payment
    }
  `);

    this.addQueryResolver<{ id: string }, IPayment>('getPayment',
      ({ args, user }) => new PaymentRepository(user, args.id).getData(), ['admin', 'user']);

    this.addEntityResolver<IPayment, IOrder>('Payment', 'order',
      async ({ parent, user }) => {
        return new OrderRepository(user, parent.orderId).getData();
      });
  }
}

export const paymentResolver = new PaymentResolver();
