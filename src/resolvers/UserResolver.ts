import { ICartElement } from '../entities/ICartElement';
import { IEntity } from '../entities/IEntity';
import { ILicenseKey } from '../entities/ILicenseKey';
import { IOrder } from '../entities/IOrder';
import { IProduct } from '../entities/IProduct';
import { IUser } from '../entities/IUser';
import CartRepository from '../repositories/CartRepository';
import LicenseRepository from '../repositories/LicenseRepository';
import ProductRepository from '../repositories/ProductRepository';
import { UserRepository } from '../repositories/UserRepository';
import { BaseResolver } from './baseResolvers';

class UserResolver extends BaseResolver {
  setResolvers(): void {
    this.setSchema(`
    type CartElement {
      id: ID
      count: Int
      licenseKey: LicenseKey
      product: Product
      name: String
      createdAt: String
      updatedAt: String
    }

    input CartElementInput {
      productId: ID
      licenseKeyId: ID
      count: Int
      name: String
    }

    input CartElementUpdateInput {
      count: Int
      name: String
    }

    type UserResDto {
        id: ID
        name: String
    }
    
    type ShortUserResDto {
        id: ID
        name: String
    }
    
    type User {
        id: ID
        name: String
    }

    type UserMe {
        id: ID
        name: String
        role: String
        orders: [Order]
        licenses: [LicenseKey]
        cart: [CartElement]
    }

    input UserInput {
        name: String
        email: String
        password: String
    }
    
    input UserUpdateInput {
        name: String
        email: String
        password: String
    }
    
    type Query {
      getUser(id: ID!): ShortUserResDto!
      getUsers: [ShortUserResDto]!
      me: UserMe!
    }
  
    type Mutation {
      createUser(user: UserInput!): User!
      editUser(id: ID!, user: UserInput!): User
      editMe(user: UserInput!): User
      deleteUser(id: ID!): Boolean
      addCartElement(input: CartElementInput!): CartElement
      removeCartElement(id: ID!): Boolean
      editCartElement(id: ID!, input: CartElementUpdateInput!): CartElement
      orderCart: Order
      clearCart: Boolean
    }
  `);

    this.addQueryResolver<{}, IUser[]>('getUsers', 
      ({ args, user }) => new UserRepository(user).getAll(), ['admin']);

    this.addQueryResolver<{ id: string }, IUser | null>('getUser', 
      async ({ args, user }) => new UserRepository(user, args.id).getData(), ['admin']);

    this.addQueryResolver<{}, IUser | null>('me', async ({ args, user }) => {
      if (!user) return null;
      return user;
    }, ['admin', 'user']);

    this.addMutationResolver<{ id: string, user: IUser }, IEntity>('editUser', 
      async ({ args, user }) => new UserRepository(user, args.id).edit(args.user), ['admin']);

    this.addMutationResolver<{ user: IUser }, IEntity>('editMe',
      async ({ args, user }) => new UserRepository(user, user.id).edit(args.user), ['admin', 'user']);

    this.addMutationResolver<{ id: string }, boolean>('deleteUser',
      async ({ args, user }) => new UserRepository(user, args.id).delete(), ['admin']);

    this.addMutationResolver<{ input: ICartElement }, ICartElement>('addCartElement',
      async ({ args, user }) => new CartRepository(user).create(args.input), ['admin', 'user']);

    this.addMutationResolver<{ id: string }, boolean>('removeCartElement',
      async ({ args, user }) => new CartRepository(user, args.id).delete(), ['admin', 'user']);

    this.addMutationResolver<{ id: string, input: ICartElement }, ICartElement>('editCartElement',
      async ({ args, user }) => new CartRepository(user, args.id).edit(args.input), ['admin', 'user']);

    this.addMutationResolver<{}, IOrder>('orderCart',
      async ({ args, user }) => new CartRepository(user).order(), ['admin', 'user']);

    this.addMutationResolver<{}, boolean>('clearCart',
      async ({ args, user }) => new CartRepository(user).clear(), ['admin', 'user']);

    this.addEntityResolver<IUser, IOrder[]>('UserMe', 'orders',
      async ({ parent, user }) => new UserRepository(user, parent.id).getOrders());

    this.addEntityResolver<IUser, ILicenseKey[]>('UserMe', 'licenses',
      async ({ parent, user }) => new UserRepository(user, parent.id).getLicenses());

    this.addEntityResolver<IUser, ICartElement[]>('UserMe', 'cart',
      async ({ parent, user }) => new UserRepository(user, parent.id).getCart());

    this.addEntityResolver<ICartElement, ILicenseKey>('CartElement', 'licenseKey',
      async ({ parent, user }) => new LicenseRepository(user, parent.licenseKeyId).getData());

    this.addEntityResolver<ICartElement, IProduct>('CartElement', 'product',
      async ({ parent, user }) => new ProductRepository(user, parent.productId).getData());
  }
}

export const userResolver = new UserResolver();
