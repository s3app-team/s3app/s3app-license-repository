import { IOrder } from '../entities/IOrder';
import { IProduct } from '../entities/IProduct';
import OrderRepository from '../repositories/OrderRepository';
import ProductRepository from '../repositories/ProductRepository';
import { BaseResolver } from './baseResolvers';

class ProductResolver extends BaseResolver {
  setResolvers(): void {
    this.setSchema(`
    scalar DateTime

    type Product {
      id: ID
      name: String
      price: Float
      days: Int
      createdAt: DateTime
      updatedAt: DateTime
    }

    input ProductInput {
      name: String
      price: Float
      days: Int
    }

    type Query {
      getProduct(id: ID!): Product
      getProducts: [Product]
    }
  
    type Mutation {
      addProduct(input: ProductInput!): Product
      editProduct(id: ID!, input: ProductInput!): Product
      deleteProduct(id: ID!): Boolean
    }
  `);

    this.addQueryResolver<{ id: string }, IProduct>('getProduct',
      ({ args, user }) => new ProductRepository(user, args.id).getData(), ['admin', 'user', 'guest']);

    this.addQueryResolver<{}, IProduct[]>('getProducts',
      ({ args, user }) => new ProductRepository(user).getAll(), ['admin', 'user', 'guest']);

    this.addMutationResolver<{ input: IProduct }, IProduct>('addProduct',
      async ({ args, user }) => new ProductRepository(user).create(args.input), ['admin']);

    this.addMutationResolver<{ id: string, input: IProduct }, IProduct>('editProduct',
      async ({ args, user }) => new ProductRepository(user, args.id).edit(args.input), ['admin']);

    this.addMutationResolver<{ id: string }, boolean>('deleteProduct',
      async ({ args, user }) => new ProductRepository(user, args.id).delete(), ['admin']);
  }
}

export const productResolver = new ProductResolver();
