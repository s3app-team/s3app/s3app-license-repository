import { Config } from './config.sample';

const config:Config = {
  db: {
    connection: process.env.DB_CONNECTION!,
  },
  emailTransporter: {
    host: process.env.EMAIL_HOST!,
    port: parseInt(process.env.EMAIL_PORT!),
    auth: {
      user: process.env.EMAIL_USER!,
      pass: process.env.EMAIL_PASSWORD!,
    },
  },
  host: process.env.HOST!,
  jwt: {
    secret_key: process.env.JWT_SECRET_KEY!,
  },
  payments: {
    lifePay: {
      clientId: process.env.PAYMENTS_LIFE_PAY_CLIENT_ID!,
      clientSecret: process.env.PAYMENTS_LIFE_PAY_CLIENT_SECRET!,
    },
  },
};

export default config;