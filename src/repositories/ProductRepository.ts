import db from '../db';
import { IClient } from '../entities/IClient';
import { IOrder } from '../entities/IOrder';
import { IOrderElement } from '../entities/IOrderElement';
import { IProduct } from '../entities/IProduct';
import BaseRepository from './baseRepository';

class ProductRepository extends BaseRepository<IProduct> {
  getTable(): string {
    return 'Product';
  }

  async create(entity: Partial<IProduct>): Promise<IProduct> {
    if (entity.days as number < 1) {
      throw new Error('Days must be greater than 0');
    }
    return super.create(entity);
  }

  async edit(entity: Partial<IProduct>): Promise<IProduct> {
    if (entity.days !== undefined && entity.days as number < 1) {
      throw new Error('Days must be greater than 0');
    }
    return super.edit(entity);
  }
}

export default ProductRepository;