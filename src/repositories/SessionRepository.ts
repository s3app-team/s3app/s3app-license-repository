import { ISession } from '../entities/ISession';
import BaseRepository from './baseRepository';

class SessionRepository extends BaseRepository<ISession> {
  getTable(): string {
    return 'Session';
  }

  async check(userId: string): Promise<boolean> {
    const data = await this.getData();
    if (data.userId !== userId) {
      throw new Error('Session is not valid');
    }
    if (data.expireAt < new Date()) {
      await this.delete();
      throw new Error('Session expired');
    }
    return true;
  }
}

export default SessionRepository;