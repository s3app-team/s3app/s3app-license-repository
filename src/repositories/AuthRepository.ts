import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { UserRepository } from './UserRepository';
import config from '../config/config';
import { ConfirmEmailRepository } from './ConfirmEmailRepository';
import { IUser, UserRole } from '../entities/IUser';
import SessionRepository from './SessionRepository';

export class AuthRepository extends UserRepository {

  public async signUp(userForm: Partial<IUser>) {
    if (this.contextUser) {
      throw new Error('You are already registered');
    }

    if (!userForm.email) {
      throw new Error('Email is not specified');
    }
    if (!userForm.name) {
      throw new Error('Name is not specified');
    }
    if (!userForm.password) {
      throw new Error('Password is not specified');
    }
    if (!userForm.acceptedPrivacy) {
      throw new Error('You must accept the privacy policy');
    }

    const user:Partial<IUser> = {
      email: '',
      confirmEmail: userForm.email!.trim().toLowerCase(),
      name: userForm.name,
      password: userForm.password,
      role: UserRole.USER,
      phone: userForm.phone,
      company: userForm.company,
      acceptedPrivacy: userForm.acceptedPrivacy,
    };

    let model = await this.getByEmail(userForm.email!);
    if (model) {
      throw new Error(
        'A user with this email already exists',
      );
    }
    if (!user.password) throw new Error('The user in the database does not have a password');
    const hashed = await bcrypt.hash(user.password, 10);
    user.password = hashed;
    const createdUser = await this.create(user);
    await new ConfirmEmailRepository(this.contextUser, createdUser.id).sendEmailConfirmationLink(user.confirmEmail as string);
    // await UserRepositories.SignUp(user);
    return user;
  }

  public async signIn(email: string, password: string) {
    if (this.contextUser) {
      throw new Error('You are already signed');
    }

    const user:Partial<IUser> = {
      email: email,
      password: password,
    };
    if (user.email) {
      user.email = user.email.trim().toLowerCase();
    } else {
      throw new Error('Email не указан');
    }

    let model = await this.getByEmail(user.email);
    if (!model) {
      throw new Error('User not found');
    }
    const valid = await bcrypt.compare(user.password!, model.password!);
    if (!valid) {
      throw new Error('Пароль неверен');
    }
    const session = await new SessionRepository().create({
      userId: model.id,
      expireAt: new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30),
    });
    let token = jwt.sign({ sub: model.id, type: 'user', session_id: session.id }, config.jwt.secret_key);
    return token;
  }

  public async logOut(sessionId: string) {
    if (!this.contextUser) {
      throw new Error('You are not logged in');
    }
    return new SessionRepository(this.contextUser, sessionId).delete();
  }
}
