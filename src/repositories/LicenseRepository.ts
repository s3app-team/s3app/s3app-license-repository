import { v4 as uuidv4 } from 'uuid';
import config from '../config/config';
import { transporter } from '../email';
import { ILicenseKey } from '../entities/ILicenseKey';
import { IProduct } from '../entities/IProduct';
import ProductRepository from './ProductRepository';
import BaseRepository from './baseRepository';

class LicenseRepository extends BaseRepository<ILicenseKey> {
  getTable(): string {
    return 'LicenseKey';
  }

  async checkEntityAccess(entity: Partial<ILicenseKey>): Promise<boolean> {
    if (this.contextUser && entity.userOwnerId && entity.userOwnerId !== this.contextUser?.id) {
      throw new Error('Access denied');
    }
    return true;
  }

  static async getByLicenseKey(licenseKey: string): Promise<LicenseRepository> {
    const licenseObject = new LicenseRepository();
    const license = (await licenseObject.getByFields({ licenseKey }))[0];
    return new LicenseRepository(undefined, license.id);
  }

  async updateLicenseKey(expireAt: Date): Promise<ILicenseKey> {
    await this.edit({
      expireAt,
    });
    return this.getData();
  }

  async refreshLicenseKey(): Promise<ILicenseKey> {
    return this.edit({
      licenseKey: uuidv4(),
      activated: false,
    });
  }

  async activateLicenseKey(): Promise<ILicenseKey> {
    const data = await this.getData();
    if (data.activated) {
      throw new Error('License key already activated');
    }
    await this.edit({
      activated: true,
      activatedAt: new Date(),
    });
    return this.getData();
  }

  async getByUser(userId: string): Promise<ILicenseKey[]> {
    return this.getByFields({ userOwnerId: userId });
  }

  async sendEmail(email: string): Promise<boolean> {
    const data = await this.getData();
    const result = await transporter.sendMail({
      from: `S3APP <${config.emailTransporter.auth.user}>`,
      to: email,
      subject: `Лицензионный ключ ${data.name}`,
      text: `Ваш лицензионный ключ: ${data.licenseKey}

Введите его на сайте https://app.s3app.ru`,
    });
    if (result) {
      return true;
    }
    return false;
  }

  async getProduct(): Promise<IProduct> {
    const data = await this.getData();
    return new ProductRepository(this.contextUser, data.productId).getData();
  }
}

export default LicenseRepository;