import db from '../db';
import { IClient } from '../entities/IClient';
import { IOrder } from '../entities/IOrder';
import { IOrderElement } from '../entities/IOrderElement';
import BaseRepository from './baseRepository';

class OrderElementRepository extends BaseRepository<IOrderElement> {
  getTable(): string {
    return 'OrderElement';
  }
}

export default OrderElementRepository;