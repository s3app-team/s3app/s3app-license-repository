import axios, { AxiosResponse } from 'axios';
import config from '../config/config';
import { IPayment, IPaymentStatus } from '../entities/IPayment';
import ProductRepository from './ProductRepository';
import BaseRepository from './baseRepository';
import OrderRepository from './OrderRepository';
import { IOrder } from '../entities/IOrder';
import moment from 'moment';

class PaymentRepository extends BaseRepository<IPayment> {
  getTable(): string {
    return 'Payment';
  }  

  async checkEntityAccess(entity: Partial<IPayment>): Promise<boolean> {
    if (this.contextUser && entity.orderId) {
      const order = await new OrderRepository(this.contextUser, entity.orderId).getData();
      if (order.userId !== this.contextUser.id) {
        throw new Error('Access denied');
      }
    }
    return super.checkEntityAccess(entity);
  }

  async getPaymentAuthToken(): Promise<string> {
    let data = JSON.stringify({
      'service_id': config.payments.lifePay.clientId,
      'api_key': config.payments.lifePay.clientSecret,
    });

    let configPayment = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://api-ecom.life-pay.ru/v1/auth',
      headers: { 
        'Content-Type': 'application/json', 
        'Accept': 'application/json',
      },
      data : data,
    };

    return (await axios(configPayment)).data.jwt;
  }

  async create(entity: Partial<IPayment>): Promise<IPayment> {
    entity.status = IPaymentStatus.open;
    await super.create(entity);

    const data = JSON.stringify({
      // 'email': 'dev@life-pay.ru',
      // 'phone': 79999999999,
      'is_recurrent': false,
      'send_receipt_through': 'email',
      'url_success': config.host + '/payments/' + this.id + '/success',
      'url_error': config.host + '/payments/' + this.id + '/error',
      'order_id': this.id,
      'amount': entity.sum,
      'currency_code': 'RUB',
      'service_id': config.payments.lifePay.clientId,
      'name': 'Order #' + this.id,
      'expire_date': moment(new Date()).add(10, 'minutes').utcOffset('+0300').format('YYYY-MM-DD HH:mm:ss'),
    });

    const token = await this.getPaymentAuthToken();
    
    const paymentConfig = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://api-ecom.life-pay.ru/v1/invoices',
      headers: { 
        'Content-Type': 'application/json', 
        'Accept': 'application/json', 
        'Authorization': 'Bearer ' + token,
      },
      data : data,
    };

    let result: AxiosResponse;
    
    try {
      result = await axios(paymentConfig);
    } catch (e) {
      console.error(e);
      throw e;
    }

    await this.edit({
      paymentId: result.data.id,
      formLink: result.data.form_link,
    });

    return this.getData();
  }

  async checkPayments(): Promise<boolean> {
    try {
      const payments = await this.getByFields({ status: IPaymentStatus.open });
      for (let i in payments) {
        const payment = payments[i];
        try {
          await new PaymentRepository(this.contextUser, payment.id).checkPaymentStatus();
        } catch (e) {
          console.error(e);
        }
      }

    } catch (e) {
      console.error(e);
    }

    return true;
  }

  async checkPaymentStatus(): Promise<boolean> {
    const data = await this.getData();
    
    if (data.status === IPaymentStatus.success) {
      return false;
    }
    if (data.status === IPaymentStatus.error) {
      return false;
    }

    if (!data.paymentId) {
      throw new Error('Payment not found');
    }

    if ((data.createdAt as Date).getTime() + 10 * 60 * 1000 < new Date().getTime()) {
      await this.edit({
        status: IPaymentStatus.error,
      });
      return false;
    }

    const token = await this.getPaymentAuthToken();

    let paymentConfig = {
      method: 'get',
      maxBodyLength: Infinity,
      url: 'https://api-ecom.life-pay.ru/v1/invoices/' + data.paymentId,
      headers: { 
        'Accept': 'application/json', 
        'Authorization': 'Bearer ' + token,
      },
    };

    const result = (await axios(paymentConfig)).data;

    if (result.status === 'success') {
      await this.edit({
        status: IPaymentStatus.success,
        paidAt: new Date(),
      });
      await new OrderRepository(this.contextUser, data.orderId).buy(this.id as string);
    }

    if (result.status === 'error') {
      await this.edit({
        status: IPaymentStatus.error,
      });
    }

    return result.status === 'success';
  }

  async getOrder(): Promise<IOrder> {
    const data = await this.getData();
    return new OrderRepository(this.contextUser, data.orderId).getData();
  }

  async delete(): Promise<boolean> {
    const data = await this.getData();
    if (data.status !== IPaymentStatus.open) {
      throw new Error('Can not delete closed payment');
    }

    await this.deletePayment();

    return super.delete();
  }

  async deletePayment():Promise<boolean> {
    const data = await this.getData();
    const token = await this.getPaymentAuthToken();
    const paymentConfig = {
      method: 'delete',
      maxBodyLength: Infinity,
      url: 'https://api-ecom.life-pay.ru/v1/invoices/' + data.paymentId,
      headers: { 
        'Accept': 'application/json', 
        'Authorization': 'Bearer ' + token,
      },
    };

    await axios(paymentConfig);
    
    return true;
  }
}

export default PaymentRepository;