import db from '../db';
import { ICartElement } from '../entities/ICartElement';
import { IClient } from '../entities/IClient';
import { IOrder } from '../entities/IOrder';
import { IOrderElement } from '../entities/IOrderElement';
import LicenseRepository from './LicenseRepository';
import OrderRepository from './OrderRepository';
import BaseRepository from './baseRepository';

class CartRepository extends BaseRepository<ICartElement> {
  getTable(): string {
    return 'CartElement';
  }

  async getElements(): Promise<ICartElement[]> {
    return this.getByFields({ userId: this.contextUser?.id });
  }

  async create(data: Partial<ICartElement>): Promise<ICartElement> {
    data.userId = this.contextUser?.id;
    if (data.licenseKeyId) {
      const license = await new LicenseRepository(this.contextUser, data.licenseKeyId).getData();
      if (license.userOwnerId !== this.contextUser?.id) {
        throw new Error('Access denied');
      }
      data.productId = license.productId;
    }
    return super.create(data);
  }
  
  async checkEntityAccess(entity: Partial<ICartElement>): Promise<boolean> {
    if (entity.userId && entity.userId !== this.contextUser?.id) {
      throw new Error('Access denied');
    }

    return true;
  }

  async order(): Promise<IOrder> {
    const elements = await this.getElements();

    const orderRepository = new OrderRepository(this.contextUser);
    await orderRepository.create({ userId: this.contextUser?.id });
    for (let i in elements) {
      await orderRepository.addElement({
        productId: elements[i].productId,
        licenseKeyId: elements[i].licenseKeyId,
        count: elements[i].count,
        name: elements[i].name,
      });
      await new CartRepository(this.contextUser, elements[i].id).delete();
    }
    return orderRepository.getData();
  }

  async clear(): Promise<boolean> {
    const elements = await this.getElements();

    for (let i in elements) {
      await new CartRepository(this.contextUser, elements[i].id).delete();
    }
    return true;
  }
}

export default CartRepository;