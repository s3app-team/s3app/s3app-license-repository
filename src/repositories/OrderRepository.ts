import axios from 'axios';
import db from '../db';
import { IClient } from '../entities/IClient';
import { IOrder } from '../entities/IOrder';
import { IOrderElement } from '../entities/IOrderElement';
import LicenseRepository from './LicenseRepository';
import OrderElementRepository from './OrderElement';
import ProductRepository from './ProductRepository';
import BaseRepository from './baseRepository';
import { v4 as uuidv4 } from 'uuid';
import config from '../config/config';
import { IPayment, IPaymentStatus } from '../entities/IPayment';
import PaymentRepository from './PaymentRepository';
import { UserRole } from '../entities/IUser';

class OrderRepository extends BaseRepository<IOrder> {
  getTable(): string {
    return 'Order';
  }

  async checkEntityAccess(entity: Partial<IOrder>): Promise<boolean> {
    if (this.contextUser && entity.userId) {
      if (entity.userId !== this.contextUser.id) {
        throw new Error('Access denied');
      }
    }
    return super.checkEntityAccess(entity);
  }

  async getElements(): Promise<IOrderElement[]> {
    return new OrderElementRepository(this.contextUser).getByFields({ orderId: this.id });
  }

  async addElement(element: Partial<IOrderElement>): Promise<IOrderElement> {
    return new OrderElementRepository(this.contextUser).create({ ...element, orderId: this.id });
  }

  async createPayment(): Promise<IPayment> {
    let sum = 0;
    const elements = await this.getElements();
    for (let i in elements) {
      const element = elements[i];
      const product = await new ProductRepository(this.contextUser, element.productId).getData();
      sum += product.price * element.count;
    }

    const payment = await new PaymentRepository(this.contextUser);

    return payment.create({
      sum,
      orderId: this.id,
    });    
  }

  async startBuy(): Promise<IPayment> {
    const order = await this.getData();

    if (order.isPaid) {
      throw new Error('Order is already paid');
    }

    if (this.contextUser?.role === UserRole.ADMIN) {
      await this.buy('');
      return {
        id: '',
        orderId: '',
        paidAt: new Date(),
        paymentId: '',
        status: IPaymentStatus.success,
        sum: 0,
        formLink: config.host + '/licenses',
      };
    }

    const payment = await this.createPayment();

    return payment;
  }

  async buy(paymentId: string): Promise<boolean> {
    const order = await this.getData();

    if (order.isPaid) {
      throw new Error('Order is already paid');
    }

    const elements = await this.getElements();
    for (let i in elements) {
      const element = elements[i];
      if (element.licenseKeyId) {
        const licenseRepository = new LicenseRepository(this.contextUser, element.licenseKeyId);
        const license = await licenseRepository.getData();
        const productRepository = new ProductRepository(this.contextUser, element.productId);
        const product = await productRepository.getData();
        await licenseRepository.edit({ 
          expireAt: new Date(license.expireAt.getTime() + element.count * product.days * 24 * 60 * 60 * 1000), 
        });
      } else {
        const licenseRepository = new LicenseRepository(this.contextUser);
        const productRepository = new ProductRepository(this.contextUser, element.productId);
        const product = await productRepository.getData();
        await licenseRepository.create({ licenseKey: uuidv4(), 
          expireAt: new Date(new Date().getTime() + element.count * product.days * 24 * 60 * 60 * 1000),
          userOwnerId: order.userId,
          purchaseId: '',
          productId: element.productId,
          name: element.name,
        });
        await new OrderElementRepository(this.contextUser, element.id).edit({ licenseKeyId: licenseRepository.id });
      }
    }

    if (this.contextUser?.role !== UserRole.ADMIN) {
      this.edit({ isPaid: true, paidAt: new Date(), paymentId });
    
      await this.closePaymentsExceptThis(paymentId);
    } else {
      this.edit({ isPaid: true, paidAt: new Date() });
    }

    return true;
  }

  async closePaymentsExceptThis(paymentId: string): Promise<boolean> {
    const payments = await new PaymentRepository(this.contextUser).getByFields({ orderId: this.id });
    for (let i in payments) {
      const payment = payments[i];
      if (payment.id !== paymentId) {
        const paymentRepository = new PaymentRepository(this.contextUser, payment.id);
        try {
          await paymentRepository.deletePayment();
        } catch (e) {
          console.error(e);
        }
        await paymentRepository.edit({
          status: IPaymentStatus.error,
        });
      }
    }
    return true;
  }

  async getByUser(userId: string): Promise<IOrder[]> {
    return this.getByFields({ userId });
  }

  async getSuccessPayment(): Promise<IPayment> {
    const data = await this.getData();
    return new PaymentRepository(this.contextUser, data.paymentId).getData();
  }

  async getPayments(): Promise<IPayment[]> {
    return new PaymentRepository(this.contextUser).getByFields({ orderId: this.id });
  }

  async delete(): Promise<boolean> {
    const data = await this.getData();
    if (data.isPaid) {
      throw new Error('Can not delete paid order');
    }

    const payments = await this.getPayments();

    for (let i in payments) {
      try {
        await new PaymentRepository(this.contextUser, payments[i].id).delete();
      } catch (e) {
        console.error(e);
      }
    }

    return super.delete();
  }

}

export default OrderRepository;