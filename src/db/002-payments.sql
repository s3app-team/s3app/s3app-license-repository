CREATE TABLE IF NOT EXISTS public."Payment"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "createdAt" timestamp without time zone DEFAULT now(),
    "updatedAt" timestamp without time zone,
    "deletedAt" timestamp without time zone,
    "isDeleted" boolean NOT NULL DEFAULT false,
    "paidAt" timestamp without time zone,
    "status" character varying COLLATE pg_catalog."default",
    sum numeric,
    "orderId" uuid,
    "paymentId" character varying COLLATE pg_catalog."default",
    "formLink" character varying,
    CONSTRAINT "Payment_pkey" PRIMARY KEY (id),
    CONSTRAINT "Payment_orderId_fkey" FOREIGN KEY ("orderId")
        REFERENCES public."Order" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE IF EXISTS public."Order"
    ADD COLUMN "paymentId" uuid;
ALTER TABLE IF EXISTS public."Order"
    ADD FOREIGN KEY ("paymentId")
    REFERENCES public."Payment" (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;