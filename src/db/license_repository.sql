--
-- PostgreSQL database dump
--

-- Dumped from database version 16.1 (Debian 16.1-1.pgdg120+1)
-- Dumped by pg_dump version 16.0

-- Started on 2023-12-26 19:26:04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 844 (class 1247 OID 16399)
-- Name: client_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.client_type AS ENUM (
    'marketplace',
    'service'
);


ALTER TYPE public.client_type OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 216 (class 1259 OID 16409)
-- Name: client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client (
    name character varying NOT NULL,
    type public.client_type NOT NULL,
    secret character varying NOT NULL,
    id uuid DEFAULT gen_random_uuid() NOT NULL
);


ALTER TABLE public.client OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16392)
-- Name: license_key; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.license_key (
    purchase_id character varying NOT NULL,
    license_key text NOT NULL,
    expire_time timestamp without time zone,
    create_time timestamp without time zone,
    activated boolean DEFAULT false NOT NULL,
    activate_time timestamp without time zone,
    id uuid DEFAULT gen_random_uuid() NOT NULL
);


ALTER TABLE public.license_key OWNER TO postgres;

--
-- TOC entry 3217 (class 2606 OID 32787)
-- Name: client client_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- TOC entry 3213 (class 2606 OID 32779)
-- Name: license_key license_key_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.license_key
    ADD CONSTRAINT license_key_pkey PRIMARY KEY (id);


--
-- TOC entry 3215 (class 2606 OID 32789)
-- Name: license_key license_key_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.license_key
    ADD CONSTRAINT license_key_unique UNIQUE (license_key);


-- Completed on 2023-12-26 19:26:04

--
-- PostgreSQL database dump complete
--

