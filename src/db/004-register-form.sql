ALTER TABLE IF EXISTS public."User"
    ALTER COLUMN role SET DEFAULT 'user';

ALTER TABLE IF EXISTS public."User"
    ADD COLUMN company character varying;

ALTER TABLE IF EXISTS public."User"
    ADD COLUMN phone character varying;

ALTER TABLE IF EXISTS public."User"
    ADD COLUMN "acceptedPrivacy" boolean NOT NULL DEFAULT false;