--
-- PostgreSQL database dump
--

-- Dumped from database version 16.1 (Debian 16.1-1.pgdg120+1)
-- Dumped by pg_dump version 16.0

-- Started on 2024-08-21 12:16:00

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 848 (class 1247 OID 16391)
-- Name: client_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.client_type AS ENUM (
    'marketplace',
    'service'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 222 (class 1259 OID 24461)
-- Name: CartElement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."CartElement" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "createdAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "licenseKeyId" uuid,
    "userId" uuid,
    count integer,
    "productId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    name character varying
);


--
-- TOC entry 215 (class 1259 OID 16395)
-- Name: Client; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Client" (
    name character varying NOT NULL,
    type public.client_type NOT NULL,
    secret character varying NOT NULL,
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 216 (class 1259 OID 16401)
-- Name: LicenseKey; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."LicenseKey" (
    "purchaseId" character varying NOT NULL,
    "licenseKey" text NOT NULL,
    "expireAt" timestamp without time zone,
    "createdAt" timestamp without time zone,
    activated boolean DEFAULT false NOT NULL,
    "activatedAt" timestamp without time zone,
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "userOwnerId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "productId" uuid,
    name character varying
);


--
-- TOC entry 220 (class 1259 OID 24421)
-- Name: Order; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Order" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now(),
    "updatedAt" timestamp without time zone,
    "deletedAt" timestamp without time zone,
    "isDeleted" boolean DEFAULT false NOT NULL,
    "paidAt" timestamp without time zone,
    "isPaid" boolean DEFAULT false,
    sum numeric,
    "userId" uuid,
    "paymentId" uuid
);


--
-- TOC entry 221 (class 1259 OID 24437)
-- Name: OrderElement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."OrderElement" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "createdAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "licenseKeyId" uuid,
    "orderId" uuid,
    count integer,
    "productId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    price numeric,
    name character varying
);


--
-- TOC entry 223 (class 1259 OID 24498)
-- Name: Payment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Payment" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now(),
    "updatedAt" timestamp without time zone,
    "deletedAt" timestamp without time zone,
    "isDeleted" boolean DEFAULT false NOT NULL,
    "paidAt" timestamp without time zone,
    status character varying,
    sum numeric,
    "orderId" uuid,
    "paymentId" character varying,
    "formLink" character varying
);


--
-- TOC entry 219 (class 1259 OID 24411)
-- Name: Product; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Product" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now(),
    "updatedAt" timestamp without time zone,
    "deletedAt" timestamp without time zone,
    "isDeleted" boolean DEFAULT false NOT NULL,
    name character varying,
    price numeric,
    days integer
);


--
-- TOC entry 218 (class 1259 OID 24399)
-- Name: Session; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Session" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "userId" uuid NOT NULL,
    "createdAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "deletedAt" timestamp without time zone,
    "isDeleted" boolean DEFAULT false,
    "expireAt" timestamp without time zone,
    "clientId" uuid
);


--
-- TOC entry 217 (class 1259 OID 24388)
-- Name: User; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."User" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    email character varying(255),
    password character varying(255),
    "resetPasswordExpires" timestamp without time zone,
    "resetPasswordCode" character varying(255),
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "confirmEmailCode" character varying(255),
    "confirmEmail" character varying(255),
    "confirmEmailExpires" timestamp without time zone,
    role character varying DEFAULT 'user'::character varying,
    company character varying,
    phone character varying,
    "acceptedPrivacy" boolean DEFAULT false NOT NULL
);


--
-- TOC entry 3282 (class 2606 OID 24469)
-- Name: CartElement CartElement_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."CartElement"
    ADD CONSTRAINT "CartElement_pkey" PRIMARY KEY (id);


--
-- TOC entry 3280 (class 2606 OID 24445)
-- Name: OrderElement OrderLicenseKey_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."OrderElement"
    ADD CONSTRAINT "OrderLicenseKey_pkey" PRIMARY KEY (id);


--
-- TOC entry 3278 (class 2606 OID 24431)
-- Name: Order Order_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_pkey" PRIMARY KEY (id);


--
-- TOC entry 3284 (class 2606 OID 24507)
-- Name: Payment Payment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Payment"
    ADD CONSTRAINT "Payment_pkey" PRIMARY KEY (id);


--
-- TOC entry 3276 (class 2606 OID 24420)
-- Name: Product Product_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Product"
    ADD CONSTRAINT "Product_pkey" PRIMARY KEY (id);


--
-- TOC entry 3274 (class 2606 OID 24405)
-- Name: Session Session_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Session"
    ADD CONSTRAINT "Session_pkey" PRIMARY KEY (id);


--
-- TOC entry 3272 (class 2606 OID 24398)
-- Name: User User_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY (id);


--
-- TOC entry 3266 (class 2606 OID 16409)
-- Name: Client client_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Client"
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- TOC entry 3268 (class 2606 OID 16411)
-- Name: LicenseKey license_key_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."LicenseKey"
    ADD CONSTRAINT license_key_pkey PRIMARY KEY (id);


--
-- TOC entry 3270 (class 2606 OID 16413)
-- Name: LicenseKey license_key_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."LicenseKey"
    ADD CONSTRAINT license_key_unique UNIQUE ("licenseKey");


--
-- TOC entry 3293 (class 2606 OID 24470)
-- Name: CartElement CartElement_licenseKeyId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."CartElement"
    ADD CONSTRAINT "CartElement_licenseKeyId_fkey" FOREIGN KEY ("licenseKeyId") REFERENCES public."LicenseKey"(id);


--
-- TOC entry 3294 (class 2606 OID 24475)
-- Name: CartElement CartElement_productId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."CartElement"
    ADD CONSTRAINT "CartElement_productId_fkey" FOREIGN KEY ("productId") REFERENCES public."Product"(id);


--
-- TOC entry 3295 (class 2606 OID 24480)
-- Name: CartElement CartElement_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."CartElement"
    ADD CONSTRAINT "CartElement_userId_fkey" FOREIGN KEY ("userId") REFERENCES public."User"(id);


--
-- TOC entry 3285 (class 2606 OID 24493)
-- Name: LicenseKey LicenseKey_productId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."LicenseKey"
    ADD CONSTRAINT "LicenseKey_productId_fkey" FOREIGN KEY ("productId") REFERENCES public."Product"(id) NOT VALID;


--
-- TOC entry 3286 (class 2606 OID 24485)
-- Name: LicenseKey LicenseKey_userOwnerId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."LicenseKey"
    ADD CONSTRAINT "LicenseKey_userOwnerId_fkey" FOREIGN KEY ("userOwnerId") REFERENCES public."User"(id) NOT VALID;


--
-- TOC entry 3290 (class 2606 OID 24446)
-- Name: OrderElement OrderElement_productId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."OrderElement"
    ADD CONSTRAINT "OrderElement_productId_fkey" FOREIGN KEY ("productId") REFERENCES public."Product"(id);


--
-- TOC entry 3291 (class 2606 OID 24451)
-- Name: OrderElement OrderLicenseKey_licenseKeyId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."OrderElement"
    ADD CONSTRAINT "OrderLicenseKey_licenseKeyId_fkey" FOREIGN KEY ("licenseKeyId") REFERENCES public."LicenseKey"(id);


--
-- TOC entry 3292 (class 2606 OID 24456)
-- Name: OrderElement OrderLicenseKey_orderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."OrderElement"
    ADD CONSTRAINT "OrderLicenseKey_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES public."Order"(id);


--
-- TOC entry 3288 (class 2606 OID 24513)
-- Name: Order Order_paymentId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_paymentId_fkey" FOREIGN KEY ("paymentId") REFERENCES public."Payment"(id) NOT VALID;


--
-- TOC entry 3289 (class 2606 OID 24432)
-- Name: Order Order_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_userId_fkey" FOREIGN KEY ("userId") REFERENCES public."User"(id);


--
-- TOC entry 3296 (class 2606 OID 24508)
-- Name: Payment Payment_orderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Payment"
    ADD CONSTRAINT "Payment_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES public."Order"(id);


--
-- TOC entry 3287 (class 2606 OID 24406)
-- Name: Session Session_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Session"
    ADD CONSTRAINT "Session_userId_fkey" FOREIGN KEY ("userId") REFERENCES public."User"(id);


-- Completed on 2024-08-21 12:16:01

--
-- PostgreSQL database dump complete
--

