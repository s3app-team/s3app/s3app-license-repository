CREATE TABLE IF NOT EXISTS public."User"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    name character varying(255) COLLATE pg_catalog."default",
    email character varying(255) COLLATE pg_catalog."default",
    password character varying(255) COLLATE pg_catalog."default",
    "resetPasswordExpires" timestamp without time zone,
    "resetPasswordCode" character varying(255) COLLATE pg_catalog."default",
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "confirmEmailCode" character varying(255) COLLATE pg_catalog."default",
    "confirmEmail" character varying(255) COLLATE pg_catalog."default",
    "confirmEmailExpires" timestamp without time zone,
    role character varying COLLATE pg_catalog."default" DEFAULT USER,
    CONSTRAINT "User_pkey" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public."Session"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "userId" uuid NOT NULL,
    "createdAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "deletedAt" timestamp without time zone,
    "isDeleted" boolean DEFAULT false,
    "expireAt" timestamp without time zone,
    "clientId" uuid,
    CONSTRAINT "Session_pkey" PRIMARY KEY (id),
    CONSTRAINT "Session_userId_fkey" FOREIGN KEY ("userId")
        REFERENCES public."User" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS public."Product"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "createdAt" timestamp without time zone DEFAULT now(),
    "updatedAt" timestamp without time zone,
    "deletedAt" timestamp without time zone,
    "isDeleted" boolean NOT NULL DEFAULT false,
    name character varying COLLATE pg_catalog."default",
    price numeric,
    CONSTRAINT "Product_pkey" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public."Order"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "createdAt" timestamp without time zone DEFAULT now(),
    "updatedAt" timestamp without time zone,
    "deletedAt" timestamp without time zone,
    "isDeleted" boolean NOT NULL DEFAULT false,
    "paidAt" timestamp without time zone,
    "isPaid" boolean DEFAULT false,
    sum numeric,
    "userId" uuid,
    CONSTRAINT "Order_pkey" PRIMARY KEY (id),
    CONSTRAINT "Order_userId_fkey" FOREIGN KEY ("userId")
        REFERENCES public."User" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

CREATE TABLE IF NOT EXISTS public."OrderElement"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "createdAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "licenseKeyId" uuid,
    "orderId" uuid,
    count integer,
    "productId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    price numeric,
    name character varying COLLATE pg_catalog."default",
    CONSTRAINT "OrderLicenseKey_pkey" PRIMARY KEY (id),
    CONSTRAINT "OrderElement_productId_fkey" FOREIGN KEY ("productId")
        REFERENCES public."Product" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT "OrderLicenseKey_licenseKeyId_fkey" FOREIGN KEY ("licenseKeyId")
        REFERENCES public."LicenseKey" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT "OrderLicenseKey_orderId_fkey" FOREIGN KEY ("orderId")
        REFERENCES public."Order" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS public."CartElement"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "createdAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "licenseKeyId" uuid,
    "userId" uuid,
    count integer,
    "productId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    name character varying COLLATE pg_catalog."default",
    CONSTRAINT "CartElement_pkey" PRIMARY KEY (id),
    CONSTRAINT "CartElement_licenseKeyId_fkey" FOREIGN KEY ("licenseKeyId")
        REFERENCES public."LicenseKey" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT "CartElement_productId_fkey" FOREIGN KEY ("productId")
        REFERENCES public."Product" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT "CartElement_userId_fkey" FOREIGN KEY ("userId")
        REFERENCES public."User" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

ALTER TABLE IF EXISTS public.Client
    RENAME TO "Client";

ALTER TABLE IF EXISTS public."LicenseKey"
    RENAME TO "LicenseKey";

ALTER TABLE IF EXISTS public."LicenseKey"
    RENAME purchase_id TO "purchaseId";

ALTER TABLE IF EXISTS public."LicenseKey"
    RENAME license_key TO "licenseKey";

ALTER TABLE IF EXISTS public."LicenseKey"
    RENAME expire_time TO "expireAt";

ALTER TABLE IF EXISTS public."LicenseKey"
    RENAME create_time TO "createdAt";

ALTER TABLE IF EXISTS public."LicenseKey"
    RENAME activate_time TO "activatedAt";

ALTER TABLE IF EXISTS public."LicenseKey"
    ADD COLUMN "userOwnerId" uuid;

ALTER TABLE IF EXISTS public."LicenseKey"
    ADD FOREIGN KEY ("userOwnerId")
    REFERENCES public."User" (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

ALTER TABLE public."LicenseKey"
    ADD COLUMN "isDeleted" boolean DEFAULT false,
    ADD COLUMN "deletedAt" timestamp without time zone,
    ADD COLUMN "updatedAt" timestamp without time zone;

ALTER TABLE public."Client"
    ADD COLUMN "isDeleted" boolean DEFAULT false,
    ADD COLUMN "deletedAt" timestamp without time zone,
    ADD COLUMN "updatedAt" timestamp without time zone,
    ADD COLUMN "createdAt" timestamp without time zone DEFAULT now();


ALTER TABLE IF EXISTS public."LicenseKey"
    ADD COLUMN "productId" uuid;

ALTER TABLE IF EXISTS public."LicenseKey"
    ADD FOREIGN KEY ("productId")
    REFERENCES public."Product" (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

ALTER TABLE IF EXISTS public."LicenseKey"
    ADD COLUMN name character varying;